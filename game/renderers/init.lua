local Engine = require('engine')

local HealthBar = require('game.renderers.health_bar')
Engine.registerRenderer('HealthBar', HealthBar)

local Score = require('game.renderers.score')
Engine.registerRenderer('Score', Score)
