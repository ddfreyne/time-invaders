local Engine = require('engine')
local Components = require('game.components')

local Score = {}

local lg = love.graphics

local assets = {
  font = lg.newFont('game/assets/UI pack Space/Fonts/kenvector_future_thin.ttf', 18),
}

function Score.draw(entity)
  local monitoredEntity = entity:get(Components.MonitoredEntity)
  if not monitoredEntity then return end

  local score = monitoredEntity.entity:get(Components.Score)
  if not score then return end

  local lifetime = monitoredEntity.entity:get(Engine.Components.Lifetime)
  if not lifetime then return end

  lg.setFont(assets.font)
  lg.print(string.format('SCORE %i + %.02f TIME BONUS', score.kills, score.timeBonus), 0, 0)
end

return Score
