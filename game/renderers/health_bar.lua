local Engine = require('engine')
local Components = require('game.components')

local HealthBar = {}

local lg = love.graphics

local assets = {
  greenSquare = lg.newImage('game/assets/UI pack Space/PNG/squareGreen.png'),
  redSquare   = lg.newImage('game/assets/UI pack Space/PNG/squareRed.png'),
}

function HealthBar.draw(entity)
  local monitoredEntity = entity:get(Components.MonitoredEntity)
  if not monitoredEntity then return end

  local health = monitoredEntity.entity:get(Components.Health)
  if not health then return end

  local w = assets.greenSquare:getWidth()
  local s = 5

  local factor = health.cur / health.max

  local size = entity:get(Engine.Components.Size)

  local maxCount = math.floor((size.width + s) / (w + s))
  local curCount = math.floor(factor * maxCount)

  for i = 1, maxCount do
    local image = i <= curCount and assets.greenSquare or assets.redSquare
    lg.draw(image, (i-1) * (w+s), 0)
  end
end

return HealthBar
