local Engine = require('engine')

local Offscreen = Engine.System.newType()

function Offscreen.new(entities)
  local requiredComponentTypes = {
    Engine.Components.Position,
  }

  return Engine.System.new(Offscreen, entities, requiredComponentTypes)
end

function Offscreen:updateEntity(entity, dt)
  local position = entity:get(Engine.Components.Position)

  -- Limit X range
  local minX, maxX = 50, love.window.getWidth() - 50
  if position.x < minX then position.x = minX end
  if position.x > maxX then position.x = maxX end

  -- Remove when off screen
  if position.y < -50 or position.y > love.window.getHeight() + 50 then
    self.entities:remove(entity)
  end
end

return Offscreen
