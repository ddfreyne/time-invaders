local Engine = require('engine')
local Game_Components = require('game.components')

local Timewarp = Engine.System.newType()

function Timewarp.new(entities)
  local requiredComponentTypes = {
    Engine.Components.Position,
    Engine.Components.Timewarp,
  }

  return Engine.System.new(Timewarp, entities, requiredComponentTypes)
end

-- FIXME: Cache this per run
local function _timeExplosions(entities)
  local timeExplosions = {}

  for entity in entities:pairs() do
    local timeExplosion = entity:get(Game_Components.TimeExplosion)
    if timeExplosion then
      table.insert(timeExplosions, entity)
    end
  end

  return timeExplosions
end

function Timewarp:updateEntity(entity, dt)
  local position = entity:get(Engine.Components.Position)
  local timewarp = entity:get(Engine.Components.Timewarp)

  local timeExplosions = _timeExplosions(self.entities)
  local factor = 1.0
  for _, timeExplosion in pairs(timeExplosions) do
    local timeExplosionPosition = timeExplosion:get(Engine.Components.Position)
    local distance = timeExplosionPosition:vectorTo(position):getLength()
    local distanceFactor = distance < 400 and (distance + 150) / 550 or 1.0
    factor = factor * distanceFactor
  end

  timewarp.factor = factor
end

return Timewarp
