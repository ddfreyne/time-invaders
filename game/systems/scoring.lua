local Engine = require('engine')
local Explosion = require('game.prefabs.explosion')
local Components = require('game.components')
local Signal = require('engine.vendor.hump.signal')

local Scoring = {}
Scoring.__index = Scoring

function Scoring.new(entities)
  local t = {
    entities     = entities,
    diedEntities = {},
    callbacks    = {},
  }

  local entityDied = function(entity)
    table.insert(t.diedEntities, entity)
  end
  t.callbacks.entityDied = entityDied
  -- TODO: Do not hardcode this constant
  Signal.register('game:systems:health:entity-died', entityDied)

  return setmetatable(t, Scoring)
end

function Scoring:leave()
  Signal.remove('game:systems:health:entity-died', self.callbacks.entityDied)
end

function Scoring:update(dt)
  local ship   = self.entities:firstWithComponent(Engine.Components.Input)
  local player = self.entities:firstWithComponent(Components.Score)

  local score    = player:get(Components.Score)
  local lifetime = player:get(Engine.Components.Lifetime)

  score.timeBonus = 30 - lifetime.value
  if score.timeBonus < 0 then score.timeBonus = 0 end

  for _, diedEntity in ipairs(self.diedEntities) do
    local worth = diedEntity:get(Components.Worth)
    if worth then
      score.kills = score.kills + worth.value
    end
  end

  if ship then
    local health = ship:get(Components.Health)
    score.healthBonus = health.cur
  else
    score.healthBonus = 0
  end

  score.total = score.kills + score.timeBonus + score.healthBonus

  self.diedEntities = {}
end

return Scoring
