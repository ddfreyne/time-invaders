local Engine     = require('engine')
local Components = require('game.components')

local AI = Engine.System.newType()

function AI.new(entities)
  local requiredComponentTypes = {
    Components.AI,
  }

  return Engine.System.new(AI, entities, requiredComponentTypes)
end

function AI:updateEntity(entity, dt)
  local ai = entity:get(Components.AI)

  ai.offset = ai.offset + dt

  if math.floor(ai.offset / ai.speed) % 2 == 0 then
    entity:add(Engine.Components.Velocity, 10, 0)
  else
    entity:add(Engine.Components.Velocity, -10, 0)
  end
end

return AI
