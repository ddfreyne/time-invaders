local Engine = require('engine')
local Debugger = require('debugger')

local Gamestate = require('engine.vendor.hump.gamestate')

local Components = require('game.components')
local TimeMissile = require('game.prefabs.time_missile')

local Input = {}
Input.__index = Input

function Input.new(entities)
  return setmetatable({ entities = entities }, Input)
end

function Input:update(dt)
  local entity = self.entities:firstWithComponent(Engine.Components.Input)
  if not entity then return end

  -- Move left/right
  if love.keyboard.isDown("left") then
    entity:add(Engine.Components.Velocity, -400, 0)
  elseif love.keyboard.isDown("right") then
    entity:add(Engine.Components.Velocity, 400, 0)
  else
    entity:add(Engine.Components.Velocity, 0, 0)
  end

  -- Fire bullets
  local gun = entity:get(Components.Gun)
  if gun then
    gun.isShooting = love.keyboard.isDown(" ")
  end
end

function Input:keypressed(key, isrepeat)
  local entity = self.entities:firstWithComponent(Engine.Components.Input)
  if not entity then return end

  -- Open debugger
  if key == "tab" then
    Gamestate.push(Debugger.Gamestate.new(self.entities))
    return
  end

  -- Fire time missile
  if key == 'b' then
    local position = entity:get(Engine.Components.Position)
    local timeMissile = TimeMissile.new()
    timeMissile:add(Engine.Components.Position, position.x, position.y)
    self.entities:add(timeMissile)
  end
end

return Input
