local Engine     = require('engine')
local Components = require('game.components')

local Shooting = Engine.System.newType()

function Shooting.new(entities)
  local requiredComponentTypes = {
    Engine.Components.Position,
    Components.Gun,
  }

  return Engine.System.new(Shooting, entities, requiredComponentTypes)
end

function Shooting:updateEntity(entity, dt)
  local position = entity:get(Engine.Components.Position)
  local gun      = entity:get(Components.Gun)

  if not gun.isShooting then return end

  if gun.cooldown <= 0 then
    local bullet = gun.bulletPrefab.new()
    bullet:add(Engine.Components.Position, position.x, position.y)
    self.entities:add(bullet)

    gun.cooldown = gun.rate
  else
    gun.cooldown = gun.cooldown - dt
  end
end

return Shooting
