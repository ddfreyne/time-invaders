local Engine = require('engine')
local Explosion = require('game.prefabs.explosion')
local Components = require('game.components')
local Signal = require('engine.vendor.hump.signal')

local Health = Engine.System.newType()

function Health.new(entities)
  local requiredComponentTypes = {
    Components.Health,
  }

  return Engine.System.new(Health, entities, requiredComponentTypes)
end

local function _playerScore(entities)
  local scoreEntity = entities:firstWithComponent(Components.Score)
  if not scoreEntity then return end

  return scoreEntity:get(Components.Score)
end

function Health:updateEntity(entity, dt)
  local health = entity:get(Components.Health)
  local worth  = entity:get(Components.Worth)
  local score = _playerScore(self.entities)

  if health.cur <= 0 then
    -- Explode
    local position = entity:get(Engine.Components.Position)
    local deathExplosion = entity:get(Components.DeathExplosion)
    if deathExplosion and position then
      local explosion = Explosion.new(deathExplosion.isBig)
      explosion:add(Engine.Components.Position, position.x, position.y)
      self.entities:add(explosion)
    end

    -- Signal death
    Signal.emit('game:systems:health:entity-died', entity)

    -- Remove
    self.entities:remove(entity)
  end
end

return Health
