local Gamestate  = require('engine.vendor.hump.gamestate')
local Engine     = require('engine')
local Components = require('game.components')

local GameEnd = {}
GameEnd.__index = GameEnd

function GameEnd.new(entities)
  return setmetatable({ entities = entities }, GameEnd)
end

function GameEnd:update(dt)
  local endEvent = self.entities:firstWithComponent(Components.EndEvent)
  if endEvent then
    local endEventLifetime   = endEvent:get(Engine.Components.Lifetime)
    local endEventAllegiance = endEvent:get(Components.Allegiance)
    local score = self.entities:firstWithComponent(Components.Score):get(Components.Score)
    if endEventLifetime.value > 2.0 then
      if endEventAllegiance.value == 'player' then
        local WinState = require('game.gamestates.win')
        Gamestate.switch(WinState.new(score))
      else
        local GameOverState = require('game.gamestates.game_over')
        Gamestate.switch(GameOverState.new(score))
      end
    end

    return
  end

  local numPlayerEntities = 0
  local numEnemyEntities  = 0

  for entity in self.entities:pairs() do
    if entity:get(Engine.Components.Input) then
      numPlayerEntities = numPlayerEntities + 1
    end

    if entity:get(Components.AI) then
      numEnemyEntities = numEnemyEntities + 1
    end
  end

  if numPlayerEntities == 0 or numEnemyEntities == 0 then
    local endEvent = Engine.Entity.new()
    endEvent:add(Engine.Components.Description, 'Event event')
    endEvent:add(Engine.Components.Lifetime     )
    endEvent:add(Components.EndEvent            )
    self.entities:add(endEvent)

    if numPlayerEntities == 0 then
      endEvent:add(Components.Allegiance, 'enemy')
    end

    if numEnemyEntities == 0 then
      endEvent:add(Components.Allegiance, 'player')
    end
  end
end

return GameEnd
