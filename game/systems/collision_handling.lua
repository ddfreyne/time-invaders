local Engine = require('engine')
local Signal = require('engine.vendor.hump.signal')
local Explosion = require('game.prefabs.explosion')
local Components = require('game.components')

local CollisionHandler = {}
CollisionHandler.__index = CollisionHandler

function CollisionHandler.new(entities)
  local t = {
    entities   = entities,
    collisions = {},
    callbacks  = {},
  }

  local collisionDetected = function(entityA, entityB)
    table.insert(t.collisions, { entityA, entityB })
  end
  t.callbacks.collisionDetected = collisionDetected
  -- TODO: Do not hardcode this constant
  Signal.register('game:systems:collision:detected', collisionDetected)

  return setmetatable(t, CollisionHandler)
end

function CollisionHandler:leave()
  Signal.remove('game:systems:collision:detected', self.callbacks.collisionDetected)
end

function CollisionHandler:update(dt)
  for _, pair in ipairs(self.collisions) do
    self:pairDetected(pair[1], pair[2])
  end
  self.collisions = {}
end

function CollisionHandler:pairDetected(a, b)
  if self:pairInteracts(a, b) then
    self:singleDetected(a, b)
  end
end

function CollisionHandler:pairInteracts(a, b)
  if a:get(Components.Bullet) and b:get(Components.Bullet) then
    return false
  end

  local aCollideable = a:get(Components.Collideable)
  local bCollideable = b:get(Components.Collideable)
  if not aCollideable or not bCollideable then
    return false
  end

  return true
end

function CollisionHandler:singleDetected(entity, otherEntity)
  local health = entity:get(Components.Health)
  if not health then return end

  local otherStrength = otherEntity:get(Components.Strength)
  if not otherStrength then return end

  local allegiance = entity:get(Components.Allegiance)
  local otherAllegiance = otherEntity:get(Components.Allegiance)
  if not allegiance or not otherAllegiance then return end

  if allegiance.value == otherAllegiance.value then return end

  health.cur = health.cur - otherStrength.value
end

return CollisionHandler
