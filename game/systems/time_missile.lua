local Engine = require('engine')
local Game_Components = require('game.components')
local TimeExplosion = require('game.prefabs.time_explosion')

local TimeMissile = Engine.System.newType()

function TimeMissile.new(entities)
  local requiredComponentTypes = {
    Engine.Components.Position,
    Game_Components.TimeMissile,
    Engine.Components.Lifetime,
  }

  return Engine.System.new(TimeMissile, entities, requiredComponentTypes)
end

function TimeMissile:updateEntity(entity, dt)
  local position    = entity:get(Engine.Components.Position)
  local timeMissile = entity:get(Game_Components.TimeMissile)
  local lifetime    = entity:get(Engine.Components.Lifetime)

  if lifetime.value > 1.5 then
    local timeExplosion = TimeExplosion.new()
    timeExplosion:add(Engine.Components.Position, position.x, position.y)
    self.entities:add(timeExplosion)

    self.entities:remove(entity)
  end
end

return TimeMissile
