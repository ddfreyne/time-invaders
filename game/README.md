# Game

Here’s a bunch of things that still need to be done:

* UI with new game and quit buttons
* Background (animated would be nice)
* [**DONE**] Delay right before win/lose
* [**DONE**] Intro credits
* [**DONE**] Scoring system
* Ships moving in (tweening)

## Brainstorming

### Scoring

* [**DONE**] Different ships, different scores
* [**DONE**] Score shown on end game
* [**DONE**] Time bonus (starts at 20s and counts down)
* [**DONE**] Health bonus (-1 score per health point lost)
* [**REJECTED**] Time penalty per time bomb used
	- This is not useful because time is a mechanic in the game that should be recommended, not discouraged.
	- Time penalty is implicit when using too many time bombs.

Later:

* High scores list on win or game over
	- Not yet, because this requires storing the scores
