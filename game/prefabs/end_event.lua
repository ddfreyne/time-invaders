local Engine     = require('engine')
local Components = require('game.components')

local EventEvent = {}

function EventEvent.new()
  local self = Engine.Entity.new()

  self:add(Engine.Components.Description, 'Event event')
  self:add(Engine.Components.Lifetime     )
  self:add(Components.EndEvent            )

  return self
end

return EventEvent
