local Engine     = require('engine')
local Components = require('game.components')

local TimeMissile = {}

function TimeMissile.new()
  local self = Engine.Entity.new()

  self:add(Engine.Components.Description, 'Time missile')
  self:add(Engine.Components.Velocity,    0, -300)
  self:add(Engine.Components.Scale,       0.5)
  self:add(Engine.Components.Z,           5)
  self:add(Engine.Components.Image,       'game/assets/Space shooter/Lasers/laserBlue02.png')
  self:add(Engine.Components.Sound,       'game/assets/sounds_1/sounds/misc/blastoff.wav')
  self:add(Engine.Components.Timewarp     )
  self:add(Engine.Components.Lifetime     )
  self:add(Components.TimeMissile         )

  return self
end

return TimeMissile
