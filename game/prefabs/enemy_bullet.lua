local Engine     = require('engine')
local Components = require('game.components')

local EnemyBullet = {}

function EnemyBullet.new()
  local self = Engine.Entity.new()

  self:add(Engine.Components.Description, 'Enemy bullet')
  self:add(Engine.Components.Velocity,    0, 600)
  self:add(Engine.Components.Scale,       0.5)
  self:add(Engine.Components.Z,           -1)
  self:add(Engine.Components.Rotation,    math.pi)
  self:add(Engine.Components.Image,       'game/assets/Space shooter/Lasers/laserRed01.png')
  self:add(Engine.Components.Sound,       'game/assets/sounds_1/sounds/lasers/7.wav')
  self:add(Engine.Components.Timewarp     )
  self:add(Components.Health,             1)
  self:add(Components.Strength,           10)
  self:add(Components.DeathExplosion,     false)
  self:add(Components.Allegiance,         'enemy')
  self:add(Components.Collideable         )
  self:add(Components.Bullet              )

  return self
end

return EnemyBullet
