local Engine = require('engine')

local ScoreBar = {}

function ScoreBar.new()
  local self = Engine.Entity.new()

  self:add(Engine.Components.AnchorPoint, 0, 0)
  self:add(Engine.Components.Z,           0)
  self:add(Engine.Components.Description, 'Score bar')
  self:add(Engine.Components.Size,        200, 30)
  self:add(Engine.Components.Renderer,    'Score')

  return self
end

return ScoreBar
