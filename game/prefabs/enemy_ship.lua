local Engine      = require('engine')
local EnemyBullet = require('game.prefabs.enemy_bullet')
local Components  = require('game.components')

local EnemyShip = {}

function EnemyShip.new()
  local imageNum = love.math.random(1, 5)
  local imageColors = { 'Black', 'Blue', 'Green', 'Red' }
  local imageColor = imageColors[love.math.random(1, 4)]

  local self = Engine.Entity.new()

  self:add(Engine.Components.Description, 'Enemy ship')
  self:add(Engine.Components.Velocity,    0, 0)
  self:add(Engine.Components.Scale,       0.5)
  self:add(Engine.Components.Z,           0)
  self:add(Engine.Components.Image,       'game/assets/Space shooter/Enemies/enemy' .. imageColor .. imageNum .. '.png')
  self:add(Engine.Components.Timewarp     )
  self:add(Components.Health,             20)
  self:add(Components.Strength,           1)
  self:add(Components.DeathExplosion,     true)
  self:add(Components.Allegiance,         'enemy')
  self:add(Components.Collideable         )
  self:add(Components.Gun,                2.0 + love.math.random(), 2 * love.math.random(), true, EnemyBullet)
  self:add(Components.AI,                 love.math.random() * 10, 1.5 + love.math.random())

  return self
end

return EnemyShip
