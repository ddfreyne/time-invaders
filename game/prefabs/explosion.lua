local Engine = require('engine')

local Components = require('game.components')

local Explosion = {}

local lg = love.graphics

-- FIXME: When does this get deleted?
-- FIXME: Get rid of isBig
function Explosion.new(isBig)
  local imagePath = 'game/assets/Space shooter/Effects/star3.png'
  local config = {
    position               = { 0, 0 },
    offset                 = { 0, 0 },
    bufferSize             = isBig and 500 or 10,
    emissionRate           = 100000,
    emitterLifetime        = 0.1,
    particleLifetime       = { 0.2, 1.0 },
    colors                 = {
                               255, 255, 255, 255,
                               255, 200, 0,   255,
                               255, 0,   0,   0,
                             },
    sizes                  = { 0.3, 0.1 },
    sizeVariation          = 1,
    speed                  = { 0, 500 },
    direction              = 3 * math.pi / 2,
    spread                 = math.pi * 2,
    linearAcceleration     = { 0, 200, 0, 800 },
    rotation               = { 0, 0 },
    spin                   = { 0, 0, 0 },
    radialAcceleration     = 0,
    tangentialAcceleration = 0.0,
  }

  local self = Engine.Entity.new()

  self:add(Engine.Components.Description,    'Explosion')
  self:add(Engine.Components.Z,              1)
  self:add(Engine.Components.Sound,          'game/assets/sounds_1/sounds/explosions/3.wav')
  self:add(Engine.Components.ParticleSystem, imagePath, config, true)
  self:add(Engine.Components.Timewarp     )

  return self
end

return Explosion
