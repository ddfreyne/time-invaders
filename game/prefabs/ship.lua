local Engine     = require('engine')
local Bullet     = require('game.prefabs.bullet')
local Components = require('game.components')

local Ship = {}

function Ship.new()
  local self = Engine.Entity.new()

  self:add(Engine.Components.Input        )
  self:add(Engine.Components.Description, 'Ship')
  self:add(Engine.Components.Velocity,    0, 0)
  self:add(Engine.Components.Scale,       0.5)
  self:add(Engine.Components.Z,           0)
  self:add(Engine.Components.Image,       'game/assets/Space shooter/playerShip1_blue.png')
  self:add(Engine.Components.Timewarp     )
  self:add(Components.DeathExplosion,     true)
  self:add(Components.Health,             100)
  self:add(Components.Strength,           1)
  self:add(Components.Allegiance,         'player')
  self:add(Components.Collideable         )
  self:add(Components.Gun,                0.1, 0.0, false, Bullet)

  return self
end

return Ship
