local Engine = require('engine')

local Components = require('game.components')

local TimeExplosion = {}

local lg = love.graphics

function TimeExplosion.new()
  local imagePath = 'game/assets/Space shooter/Effects/star2.png'
  local config = {
    position               = { 0, 0 },
    offset                 = { 0, 0 },
    bufferSize             = 1000,
    emissionRate           = 100000,
    emitterLifetime        = 0.1,
    particleLifetime       = { 0.5, 2.8 },
    colors                 = {
                               255, 255, 255, 255,
                               0,   255, 255, 255,
                               0,   0,   255, 0,
                             },
    sizes                  = { 0.3, 0.1 },
    sizeVariation          = 1,
    speed                  = { 0, 200 },
    direction              = 3 * math.pi / 2,
    spread                 = math.pi * 2,
    linearAcceleration     = { 0, 0, 0, 0 },
    rotation               = { 0, 0 },
    spin                   = { 0, 0, 0 },
    radialAcceleration     = 0,
    tangentialAcceleration = 0.0,
  }

  local self = Engine.Entity.new()

  self:add(Engine.Components.Description,    'Time explosion')
  self:add(Engine.Components.Z,              2)
  self:add(Engine.Components.Sound,          'game/assets/sounds_1/sounds/explosions/5.wav')
  self:add(Engine.Components.ParticleSystem, imagePath, config, true)
  self:add(Components.TimeExplosion          )
  self:add(Engine.Components.Timewarp          )

  return self
end

return TimeExplosion
