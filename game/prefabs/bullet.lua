local Engine     = require('engine')
local Components = require('game.components')

local Bullet = {}

function Bullet.new()
  local self = Engine.Entity.new()

  self:add(Engine.Components.Description, 'Bullet')
  self:add(Engine.Components.Velocity,    0, -600)
  self:add(Engine.Components.Scale,       0.5)
  self:add(Engine.Components.Z,           -1)
  self:add(Engine.Components.Image,       'game/assets/Space shooter/Lasers/laserBlue01.png')
  self:add(Engine.Components.Sound,       'game/assets/sounds_1/sounds/lasers/6.wav')
  self:add(Engine.Components.Timewarp     )
  self:add(Components.Health,             1)
  self:add(Components.Strength,           10)
  self:add(Components.DeathExplosion,     false)
  self:add(Components.Allegiance,         'player')
  self:add(Components.Collideable         )
  self:add(Components.Bullet              )

  return self
end

return Bullet
