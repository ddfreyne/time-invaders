local Engine = require('engine')

local HealthBar = {}

function HealthBar.new()
  local self = Engine.Entity.new()

  self:add(Engine.Components.AnchorPoint, 0, 0)
  self:add(Engine.Components.Z,           0)
  self:add(Engine.Components.Description, 'Health bar')
  self:add(Engine.Components.Size,        200, 30)
  self:add(Engine.Components.Renderer,    'HealthBar')

  return self
end

return HealthBar
