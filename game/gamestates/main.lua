local ArenaSpace = require('game.spaces.arena')
local HUDSpace   = require('game.spaces.hud')
local Bullet     = require('game.prefabs.bullet')
local Ship       = require('game.prefabs.ship')
local EnemyShip  = require('game.prefabs.enemy_ship')
local HealthBar  = require('game.prefabs.health_bar')
local ScoreBar   = require('game.prefabs.score_bar')
local Gamestate  = require('engine.vendor.hump.gamestate')
local Components = require('game.components')

local Engine   = require('engine')

local lw = love.window

local Main = setmetatable({}, { __index = Engine.Gamestate })

local function createEntities()
  local entities = Engine.Types.EntitiesCollection.new()

  local player = Engine.Entity.new()
  player:add(Components.Score)
  player:add(Engine.Components.Lifetime)
  entities:add(player)

  local camera = Engine.Entity.new()
  camera:add(Engine.Components.Camera)
  camera:add(Engine.Components.Position, lw.getWidth()/2, lw.getHeight()/2)
  camera:add(Engine.Components.Z, 100)
  entities:add(camera)

  local ship = Ship.new()
  ship:add(Engine.Components.Position, 100, love.window.getHeight() - 100)
  entities:add(ship)

  for y = 100, 300, 100 do
    -- 300 -> 0   -> 0 -> 1 -> 10
    -- 200 -> 100 -> 1 -> 2 -> 20
    -- 100 -> 200 -> 2 -> 3 -> 30

    local worth = ((300 - y) / 100 + 1) * 10
    for x = 100, 900, 100 do
      local enemyShip = EnemyShip.new(x, y)
      enemyShip:add(Engine.Components.Position, x, y)
      enemyShip:add(Components.Worth, worth)
      entities:add(enemyShip)
    end
  end

  return entities
end

local function createHUDEntities(arenaEntities)
  local entities = Engine.Types.EntitiesCollection.new()

  local ship = arenaEntities:firstWithComponent(Engine.Components.Input)
  local player = arenaEntities:firstWithComponent(Components.Score)

  local healthBar = HealthBar.new()
  healthBar:add(Engine.Components.Position, 20, 20)
  healthBar:add(Components.MonitoredEntity, ship)
  entities:add(healthBar)

  local scoreBar = ScoreBar.new()
  scoreBar:add(Engine.Components.Position, 220, 23)
  scoreBar:add(Components.MonitoredEntity, player)
  entities:add(scoreBar)

  return entities
end

function Main.new()
  local t = {}

  local entities = createEntities()

  t.arenaSpace = ArenaSpace.new(entities)
  t.hudSpace = HUDSpace.new(createHUDEntities(entities))
  t.spaces = { t.arenaSpace, t.hudSpace }

  return setmetatable(t, { __index = Main })
end

return Main
