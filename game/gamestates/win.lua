local MainState = require('game.gamestates.main')
local Gamestate = require('engine.vendor.hump.gamestate')

local Win = {}
Win.__index = Win

local lg = love.graphics

function Win.new(score)
  local t = {
    titleFont    = lg.newFont('game/assets/UI pack Space/Fonts/kenvector_future.ttf', 96),
    subtitleFont = lg.newFont('game/assets/UI pack Space/Fonts/kenvector_future_thin.ttf', 18),
    score = score,
  }

  return setmetatable(t, Win)
end

function Win:keypressed(key)
  if key == ' ' then
    Gamestate.switch(MainState.new())
  end
end

function Win:draw()
  local w = love.window.getWidth()
  local h = love.window.getHeight()
  local x = 0
  local y = h / 2 - self.titleFont:getHeight() / 2 - 20

  lg.setFont(self.titleFont)
  lg.printf('YOU WIN!', 0, y, w, 'center')

  lg.setFont(self.subtitleFont)

  local kills       = string.format('%i', self.score.kills)
  local timeBonus   = string.format('%i', self.score.timeBonus)
  local healthBonus = string.format('%i', self.score.healthBonus)
  local total       = string.format('%i', self.score.total)

  lg.printf('Kills',        0     - 20, y + 140, w / 2, 'right')
  lg.printf(kills,          w / 2 + 20, y + 140, w / 2, 'left')

  lg.printf('Time bonus',   0     - 20, y + 160, w / 2, 'right')
  lg.printf(timeBonus,      w / 2 + 20, y + 160, w / 2, 'left')

  lg.printf('Health bonus', 0     - 20, y + 180, w / 2, 'right')
  lg.printf(healthBonus,    w / 2 + 20, y + 180, w / 2, 'left')

  lg.printf('Total',        0     - 20, y + 220, w / 2, 'right')
  lg.printf(total,          w / 2 + 20, y + 220, w / 2, 'left')

  lg.printf('[ Press space to go again ]', 0, y + 300, w, 'center')
end

return Win
