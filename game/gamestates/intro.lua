local MainState = require('game.gamestates.main')
local Gamestate = require('engine.vendor.hump.gamestate')

local flux = require('game.vendor.flux.flux')

local Intro = {}
Intro.__index = Intro

local lg = love.graphics

function Intro.new()
  local t = {
    presents   = lg.newImage('game/assets/intro/intro-0.png'),
    logo       = lg.newImage('game/assets/intro/intro-1.png'),
    blurryLogo = lg.newImage('game/assets/intro/intro-2.png'),
    presentsOpacity   = 0,
    logoOpacity       = 0,
    blurryLogoOpacity = 0,
  }

  -- 1
  flux.to(t, 2, { presentsOpacity   = 255 }):ease("linear"):delay(0)

  -- 2
  flux.to(t, 2, { presentsOpacity   = 0   }):ease("linear"):delay(3)
  flux.to(t, 2, { logoOpacity       = 255 }):ease("linear"):delay(4)

  -- 3
  flux.to(t, 3, { logoOpacity       = 0   }):ease("linear"):delay(7)
  flux.to(t, 3, { blurryLogoOpacity = 255 }):ease("linear"):delay(6)
  flux.to(t, 3, { blurryLogoOpacity = 0   }):ease("linear"):delay(9):oncomplete(function() Gamestate.switch(MainState.new()) end)

  return setmetatable(t, Intro)
end

function Intro:keypressed(key)
  if key == ' ' then
    Gamestate.switch(MainState.new())
  end
end

function Intro:update(dt)
  flux.update(dt)
end

function Intro:draw()
  local w = love.window.getWidth()
  local h = love.window.getHeight()

  local x = w / 2 - self.logo:getWidth() / 2
  local y = h / 2 - self.logo:getHeight() / 2

  lg.setColor(255, 255, 255, self.presentsOpacity)
  lg.draw(self.presents, x, y)

  lg.setColor(255, 255, 255, self.blurryLogoOpacity)
  lg.draw(self.blurryLogo, x, y)

  lg.setColor(255, 255, 255, self.logoOpacity)
  lg.draw(self.logo, x, y)
end

return Intro
