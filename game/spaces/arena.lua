local InputSystem             = require('game.systems.input')
local HealthSystem            = require('game.systems.health')
local CollisionHandlingSystem = require('game.systems.collision_handling')
local ShootingSystem          = require('game.systems.shooting')
local AISystem                = require('game.systems.ai')
local GameEndSystem           = require('game.systems.game_end')
local OffscreenSystem         = require('game.systems.offscreen')
local TimewarpSystem          = require('game.systems.timewarp')
local ScoringSystem           = require('game.systems.scoring')
local TimeMissileSystem       = require('game.systems.time_missile')

local Engine = require('engine')

local Arena = {}

function Arena.new(entities)
  local systems = {
    InputSystem.new(entities),
    HealthSystem.new(entities),
    TimewarpSystem.new(entities),
    Engine.Systems.CollisionDetection.new(entities),
    CollisionHandlingSystem.new(entities),
    Engine.Systems.Physics.new(entities),
    OffscreenSystem.new(entities),
    ShootingSystem.new(entities),
    AISystem.new(entities),
    GameEndSystem.new(entities),
    Engine.Systems.Sound.new(entities),
    Engine.Systems.Lifetime.new(entities),
    TimeMissileSystem.new(entities),
    Engine.Systems.ParticleSystem.new(entities),
    ScoringSystem.new(entities),
    Engine.Systems.Rendering.new(entities),
  }

  return Engine.Space.new(entities, systems)
end

return Arena
