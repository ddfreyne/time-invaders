require('engine')

local Components = {}

Components.Health = {
  order  = 0,
  name   = 'Health',
  new    = function(max) return { cur = max, max = max } end,
  format = function(self) return '' .. self.cur .. '/' .. self.max end,
}

Components.Strength = {
  order  = 1,
  name   = 'Strength',
  new    = function(s) return { value = s } end,
  format = function(self) return '' .. self.value end,
}

Components.DeathExplosion = {
  order  = 2,
  name   = 'Death explosion',
  new    = function(isBig) return { isBig = isBig } end,
  format = function(self) return self.isBig and 'big' or 'small' end,
}

Components.Allegiance = {
  order  = 3,
  name   = 'Allegiance',
  new    = function(value) return { value = value } end,
  format = function(self) return '' .. self.value end,
}

Components.Collideable = {
  order  = 4,
  name   = 'Can collide?',
  new    = function() return {} end,
  format = function(self) return 'yes' end,
}

Components.Bullet = {
  order  = 5,
  name   = 'Is bullet?',
  new    = function() return {} end,
  format = function(self) return 'yes' end,
}

Components.Gun = {
  order  = 6,
  name   = 'Gun',
  new    = function(rate, cooldown, isShooting, bulletPrefab) return { rate = rate, cooldown = cooldown, isShooting = isShooting, bulletPrefab = bulletPrefab } end,
  format = function(self) return (self.isShooting and 'on' or 'off') .. '; rate: ' .. string.format('%3.1f', self.rate) .. '; ' .. 'cooldown: ' .. string.format('%3.1f', self.cooldown) end,
}

Components.AI = {
  order  = 7,
  name   = 'AI',
  new    = function(offset, speed) return { offset = offset, speed = speed } end,
  format = function(self) return 'offset: ' .. string.format('%3.1f', self.offset) .. '; ' .. 'speed: ' .. string.format('%3.1f', self.speed) end,
}

Components.MonitoredEntity = {
  order  = 8,
  name   = 'Monitored entity',
  new    = function(entity) return { entity = entity } end,
  format = function(self) return self.entity and 'something' or 'nothing' end,
}

Components.TimeMissile = {
  order  = 9,
  name   = 'Time missile',
  new    = function() return {} end,
  format = function(self) return 'Yes' end,
}

Components.TimeExplosion = {
  order  = 10,
  name   = 'Time explosion',
  new    = function() return {} end,
  format = function(self) return 'Yes' end,
}

Components.EndEvent = {
  order  = 11,
  name   = 'End event',
  new    = function() return {} end,
  format = function(self) return 'Yes' end,
}

Components.Worth = {
  order  = 12,
  name   = 'Worth',
  new    = function(value) return { value = value } end,
  format = function(self) return string.format('%i', self.value) end,
}

Components.Score = {
  order  = 13,
  name   = 'Score',
  new    = function() return { kills = 0, timeBonus = 0, healthBonus = 0, total = 0 } end,
  format = function(self) return string.format('%i', self.total) end,
}

return Components
