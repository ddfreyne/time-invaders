# Time Invaders

![Screenshot](screenshot-0.png)

This is a fairly simple Space Invaders clone written in [LÖVE](https://love2d.org/).

Artwork by [Kenney](http://kenney.nl/assets).

The goal of this project is to build a game to the best of my abilities and learn a lot while doing so. This project is definitely a playground; don’t expect the code in here to be consistent or production-ready. Even though I want to end up with well-written and reusable code in the end, don’t expect that to happen any time soon.

The project is divided into three parts:

* `game` includes the game itself
* `engine` includes generic code that is meant to be reusable for future projects
* `debugger` includes a debugger with inspection and basic editing functionalities

Each of these directories has its own README file with more information.
