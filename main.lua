local ProFi      = require('game.vendor.ProFi')
local Gamestate  = require('engine.vendor.hump.gamestate')
local IntroState = require('game.gamestates.intro')
require('game.renderers')

function love.load()
  -- ProFi:start()
  Gamestate.registerEvents()
  Gamestate.switch(IntroState.new())
end

function love.quit()
  -- ProFi:stop()
  -- ProFi:writeReport('profile.txt')
end
